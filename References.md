# References | Список источников

* [Open ID Connect (Code Auth Flow)](https://openid.net/specs/openid-connect-core-1_0.html#CodeFlowAuth)
* [Keycloak - Identity and Access Management for Modern Applications (Stian Thorgersen, Pedro Igor Silva)](https://www.amazon.com/Keycloak-Identity-Management-Applications-applications/dp/1804616443)
* [Keycloak, clients and roles: a tutorial (Medium Article)](https://tomas-pinto.medium.com/keycloak-clients-and-roles-a-tutorial-b334147f1dbd)
* [Securing Applications and Services Guide (Keycloak Docs)](https://www.keycloak.org/docs/latest/securing_apps/)
* [Single Sign-On (SSO): SAML, OAuth2, OIDC simplified (Medium Article)](https://medium.com/javarevisited/single-sign-on-sso-saml-oauth2-oidc-simplified-cf54b749ef39)
