1. Поднять и настроить KeyCloak
2. Поднять и настроить back
3. Поднять Vue apps


чтобы выдавать IP нужно восстановить [docker-mac-net-connect](https://github.com/chipmk/docker-mac-net-connect) </br>
Установка:
`brew install docker-mac-net-connect`


```shell
docker run \
-p 8180:8180 \
-e KEYCLOAK_ADMIN=admin \
-e KEYCLOAK_ADMIN_PASSWORD=<your_password> \
-v keycloak21_data:/opt/keycloak/data/ \
--name keycloak21 \
-d \
quay.io/keycloak/keycloak:21.0.1 start-dev --http-port=8180
```

Далее переходим на локальную 


<local_ip>:<local_port>/realms/sso-itmo/.well-known/openid-configuration